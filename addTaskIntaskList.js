function addTaskInNoFirstTaskList(taskListId, taskName, taskId){    
    const taskList = dataMock[taskListId];
    const newTaskName = taskName;  

    pushNewTaskInDataMock (taskList, taskId, newTaskName)

    let divAddTaskLast = document.querySelectorAll(`.dropdown`)[taskListId];
    divAddTaskLast.insertAdjacentHTML("beforebegin",getTaskListItemHTMLTemplate (newTaskName, taskListId, taskId) )
 
    deleteTaskFromDataMock (taskListId, taskId);
    deleteTaskFromTaskList (taskListId, taskId);
    updatesCounterTasks ();
    checkForDisableOrActiveBtnAddCard();
    dragAndDrop(taskListId)
    console.log(dataMock)
}


function deleteTaskFromTaskList (currentTaskListId, taskId){
    const taskListId = currentTaskListId-1;
    let arrWithItemsForDropDown = document.querySelectorAll(`.task`)
    for (let i=0;i<arrWithItemsForDropDown.length;i++){
        if(arrWithItemsForDropDown[i].dataset.tasklistId == taskListId ){
            if(taskId === arrWithItemsForDropDown[i].dataset.taskId){
                arrWithItemsForDropDown[i].remove()
            }    
        } 
    }    
}

function addTaskInFirstTaskList(taskListId){
    const taskList = dataMock[taskListId];
    const newTaskId = uuidv4();
    const inputField = document.querySelector('.input');
    const newTaskName = inputField.value;  

    pushNewTaskInDataMock (taskList, newTaskId, newTaskName)
    
    const divAddTaskLast = document.querySelector(".task_input");
    divAddTaskLast.insertAdjacentHTML("beforebegin",getTaskListItemHTMLTemplate (newTaskName, taskListId, newTaskId) )
    
    updatesCounterTasks ();
    checkForDisableOrActiveBtnAddCard()
    dragAndDrop (taskListId)
    checkTaskName (newTaskName, taskListId+1, newTaskId)
    console.log(dataMock)
}

function checkTaskName (name, taskListId, taskId){
    if(name == "" || name.slice(0,1) == " "){
        deleteTaskFromDataMock (taskListId, taskId);
        deleteTaskFromTaskList (taskListId, taskId);
    }
}