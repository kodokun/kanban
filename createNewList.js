
function addNewTaskList(){
    const inputField = document.querySelector(".newTask_input"); 
    const newTaskListName = inputField.value;
    if(newTaskListName == "" || newTaskListName.slice(0,1) == " "){
        return removeCreateNewTaskList()
    }
    addNewTaskListInDataMock (newTaskListName)
    updatesDataMock()  
    updatesCounterTasks ();
    uploadTask()
    creteEventsHandlerForNoFirstTaskList ()
    creteEventsHandlerForFirstTaskList (); 
    updatesAllTaskInTaskList ();
    createEventsShowBtnDeleteList();
    checkForDisableOrActiveBtnAddCard ()
    console.log(dataMock)
}


function showCreateNewTaskList () {
    const newTaskListLocation = document.querySelector(".main");
    newTaskListLocation.insertAdjacentHTML("afterbegin", getHtmlCodeForAddNewTaskList())
    const submitcreateNewListBtnLocation = document.querySelector("#buttonNewList")
    submitcreateNewListBtnLocation.addEventListener("click", addNewTaskList) 
    const inputField = document.querySelector(".newTask_input"); 
    focusOnInput(inputField)
    inputField.addEventListener("blur", addNewTaskList)
}

function removeCreateNewTaskList() {
    document.querySelector(".main_taskList").remove()
}


function getHtmlAllTaskInTaskList (){
    for(let i=0; i<dataMock.length; i++){
        for(let j=0; j<dataMock[i].issues.length; j++){
            let taskName = dataMock[i].issues[j].name;
            let taskListId = dataMock[i].taskListId;
            let taskId = dataMock[i].issues[j].id
            let htmlCode = getTaskListItemHTMLTemplate (taskName, taskListId, taskId) ;
            let divAddTaskLast = document.querySelectorAll(`.task_input`)[taskListId];
            divAddTaskLast.insertAdjacentHTML("beforebegin", htmlCode )
        }
    }
}

function deleteTaskFromTaskListForUpdates (){
    let arrWithAllTask = document.querySelectorAll(`[data-tasklist-id]`)
    for (let i=0;i<arrWithAllTask.length;i++){
        arrWithAllTask[i].remove()   
    }    
}


function updatesCreateEventHandlersOnTaskInTaskList(){  
    let arrWithAllTask = document.querySelectorAll(`.task`)
    for (let i=0;i<arrWithAllTask.length;i++){
        let taskName = arrWithAllTask[i].dataset.taskName
        arrWithAllTask[i].addEventListener("click", () => showDetailedInformationAboutTask(taskName))
    }   
}

function updatesAllTaskInTaskList (){
    deleteTaskFromTaskListForUpdates ();
    getHtmlAllTaskInTaskList ();
    updatesCreateEventHandlersOnTaskInTaskList();
    dragAndDropAll ()
}

const createNewListBtnLocation = document.querySelector(".header_newList")
createNewListBtnLocation.addEventListener("click", showCreateNewTaskList)
