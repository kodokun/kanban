const dataMock = [

    {   
        taskListId:0,
        title: 'Backlog',
        issues:[ ],
    },
    {
        taskListId:1,
        title: 'Ready',
        issues:[ ],
    },
    {
        taskListId:2,
        title: 'In Progress',
        issues:[ ],
    },
    {
        taskListId:3,
        title: 'Finished',
        issues:[ ],
    },
] 

function updatesDataMock (){
    for(let i=1;i<dataMock.length;i++){
        dataMock[i].taskListId = dataMock[i].taskListId + 1
    }
}

function addNewTaskListInDataMock ( newTaskName){
    dataMock.unshift(
        {   
            taskListId:0,
            title: newTaskName,
            issues:[ ],
        }
    )
}

function deleteTaskFromDataMock (taskListId, taskId){
    const deleteTaskListId = taskListId-1;
    for(let i=0; i<dataMock[deleteTaskListId].issues.length;i++ ){
        if (dataMock[deleteTaskListId].issues[i].id == taskId){
            dataMock[deleteTaskListId].issues.splice(i,1)
        }
    }
}

function pushNewTaskInDataMock (taskList, newTaskId, newTaskName){
    taskList.issues.push(
        {
        id: newTaskId,
        name: newTaskName,
    });
}

