function createEventsShowBtnDeleteList() {
    const btnDeleteListLocation = document.querySelectorAll(".settings_delete_btn")
    const btnShowDeleteListLocation = document.querySelectorAll(".settings_points")
    for(let i=0;i<dataMock.length; i++){
        btnShowDeleteListLocation[i].addEventListener("click", () =>showHiddenBlockBtn (btnDeleteListLocation[i], i))
        btnDeleteListLocation[i].addEventListener("click", () => deleteTaskListFromDataMock(i))
        btnDeleteListLocation[i].addEventListener("blur", () => hideDropDown(btnDeleteListLocation[i]))
    }
}

function deleteTaskListFromDataMock (taskListId) {
    dataMock.splice(taskListId,1);
    console.log(dataMock)
    updatesDataMockForDeleteList (taskListId);
    updatesCounterTasks ();
    uploadTask()
    creteEventsHandlerForNoFirstTaskList ()
    creteEventsHandlerForFirstTaskList (); 
    updatesAllTaskInTaskList ();
    createEventsShowBtnDeleteList();
    showInfAboutAllTaskListDelite();
    checkForDisableOrActiveBtnAddCard ()
}


function updatesDataMockForDeleteList (taskListId){
    for(let i = taskListId;i<dataMock.length; i++){
        dataMock[i].taskListId = i
    }
}


function showInfAboutAllTaskListDelite(){
    const informationPlace = document.querySelector(`.main`) 
    if(dataMock.length == 0){
        let htmlCode = getHtmlAllTaskListDelete ();
        informationPlace.insertAdjacentHTML("afterbegin",htmlCode )
    }
}

function removeInfAboutAllTaskListDelite(){
    const infAboutAllTaskListDeliteLocation = document.querySelectorAll(".information")
    if(infAboutAllTaskListDeliteLocation.length == 1){
        infAboutAllTaskListDeliteLocation[0].remove()
    }
}
createNewListBtnLocation.addEventListener("click", removeInfAboutAllTaskListDelite)