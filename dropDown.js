function showHiddenBlock(targetLocation){
    if(targetLocation.hidden ==true){
        targetLocation.hidden = false;
    }
}
function hideDropDown(dropDawnLocation){
    if(dropDawnLocation.hidden == false){
        dropDawnLocation.hidden = true;
    }
}
function getHtmlCodeInDropDown (taskListId){
    const previousTaksList = dataMock[taskListId-1];
    let htmlCodeInDropDown = ``;
    for(let i = 0; i < previousTaksList.issues.length;i++ ){ 
        htmlCodeInDropDown += getTaskListItemHTMLTemplateInDropDown( previousTaksList, i)
    }
    return htmlCodeInDropDown
}
function createDropDownInTaskList(dropDawnLocation, dropDawnLocationOpen, numTaskList){
    dropDawnLocation.innerHTML = getHtmlCodeInDropDown(numTaskList);
    showHiddenBlock(dropDawnLocationOpen);
}


function createEventHandlersOnTaskInDropDownList(taskListId, hiddenPlace){  
    const arrWithItemsForDropDown = document.querySelectorAll(`.dropdown_task`)
    for (let i=0;i<arrWithItemsForDropDown.length;i++){
        let taskName = arrWithItemsForDropDown[i].dataset.taskDropdownName
        let taskId = arrWithItemsForDropDown[i].dataset.taskDropdownId
        let handlerForDropDownItem = getEventHandlerForDropDownItem(taskListId, taskName, hiddenPlace, taskId)
        arrWithItemsForDropDown[i].addEventListener("click", handlerForDropDownItem) 
    }

}