function creteEventsHandlerForNoFirstTaskList () {
    for(let i=0;i<dataMock.length-1; i++){
        let divDropDown = document.querySelectorAll(`.dropdown_list`)[i+1];
        let divDropDownHidden = document.querySelectorAll(".dropdown")[i+1];
        let buttonAddCard = document.querySelectorAll('.button_add_card')[i+1];
        let fieldWithArrow = document.querySelectorAll(".dropdown_arrow")[i+1]

        buttonAddCard.addEventListener("click", () => getEventForBtnAddCardInNoFirstTaskList (divDropDown, divDropDownHidden, i+1))
        fieldWithArrow.addEventListener("click", () => hideDropDown(divDropDownHidden))
    }
}


function creteEventsHandlerForFirstTaskList () {
    if(dataMock.length>0){
        const buttonBacklog = document.querySelectorAll('.button_add_card')[0];
        const buttonSubmit = document.querySelector("#button");
        const inputLocation = document.querySelector(".input")

        buttonBacklog.addEventListener("click", () => eventButtonBacklog(true) );
        buttonSubmit.addEventListener("click", () => getEventForBtnSubmit(0, false));
        inputLocation.addEventListener("blur", () => getEventForBtnSubmit(0, false))
    }
}

function getEventForBtnSubmit(taskListId, showTaskFormAndHideFormOpennerBtn){
    addTaskInFirstTaskList(taskListId)
    eventButtonBacklog(showTaskFormAndHideFormOpennerBtn)
    createEventHandlersOnTaskInTaskList(taskListId)
}

function getEventHandlerForDropDownItem(taskListId, taskName, targetDropDown, taskId){
    return function (){
        addTaskInNoFirstTaskList(taskListId, taskName, taskId)
        hideDropDown(targetDropDown)
        createEventHandlersOnTaskInTaskList(taskListId)
    } 
} 

function getEventForBtnAddCardInNoFirstTaskList (divDropDown, divDropDownHidden, taskListId){
    createDropDownInTaskList(divDropDown,divDropDownHidden, taskListId)
    createEventHandlersOnTaskInDropDownList(taskListId, divDropDownHidden)
}

function eventButtonBacklog(showTaskFormAndHideFormOpennerBtn){ 
    const divInputFieldBacklog = document.querySelector(".task_input")
    const inputLocation = document.querySelector(".input")
    const divButtonSubmitBacklog = document.querySelector(".button_submit")
    const buttonBacklog = document.querySelectorAll('.button_add_card')[0];
    function showHideTaskForm( needHide ){
        divInputFieldBacklog.hidden = needHide;
        divButtonSubmitBacklog.hidden = needHide;
    }

    const showHideTaskFormOpenerBtn = (needHide) => buttonBacklog.hidden = needHide;

    if (showTaskFormAndHideFormOpennerBtn){
        showHideTaskForm(false);
        showHideTaskFormOpenerBtn(true);
    }else {
        showHideTaskForm(true);
        showHideTaskFormOpenerBtn(false);
    }
    focusOnInput(inputLocation)
}

