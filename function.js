function uploadTask(){
    const boxMain = document.querySelector('.main')
    const mainHtmlCode= getHtmlCodeForAllTaskList(dataMock)
    boxMain.innerHTML = mainHtmlCode;
}

// footer log
function updatesCounterActiveTasks (){
    const activeTaskLocation = document.querySelector(".footer_left_aTask")
    let counter;
    (dataMock.length == 0)?counter = 0:counter = dataMock[0].issues.length;
    activeTaskLocation.innerHTML = getHtmlCodeInActiveTask(counter)
}
function updatesCounterFinishedTasks (){
    const finishedTaskLocation = document.querySelector(".footer_left_fTask")
    let counter;
    (dataMock.length == 0 || dataMock.length == 1)?counter = 0:counter = dataMock[dataMock.length-1].issues.length;
    finishedTaskLocation.innerHTML = getHtmlCodeInFinishedTask(counter)
}

function updatesCounterTasks () {
    updatesCounterActiveTasks ()
    updatesCounterFinishedTasks ()
}


function checkForDisableOrActiveBtnAddCard () {
    const arrButtonAddCardLocation = document.querySelectorAll('.button_add_card')
    for(let i = 1;i<dataMock.length;i++){
        if(dataMock[i-1].issues.length == 0){
            arrButtonAddCardLocation[i].setAttribute("disabled", "true");
            arrButtonAddCardLocation[i].classList.remove("button_add_card_hover")
        }else {
            arrButtonAddCardLocation[i].removeAttribute("disabled");
            arrButtonAddCardLocation[i].classList.add("button_add_card_hover");
        }
    }
}


function focusOnInput(inputLocation){
    inputLocation.focus()
}


function showHiddenBlockBtn(targetLocation, i){
    if(targetLocation.hidden ==true){
        targetLocation.hidden = false;
    }
    const btnDeleteListLocation = document.querySelectorAll(".settings_delete_btn")
    focusOnInput(btnDeleteListLocation[i])
}