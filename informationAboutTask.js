function createEventHandlersOnTaskInTaskList(taskListId){  
    const arrWithItemsForAllTaskList = document.querySelectorAll(`.task`)
    const arrWithItemsForTaskList= getArrWithItemsForTaskList(arrWithItemsForAllTaskList, taskListId)
    const arrWithItemsForTaskListLastItem = arrWithItemsForTaskList.length-1
    if(arrWithItemsForTaskList[arrWithItemsForTaskListLastItem] == undefined){
        const taskName =""
    }else{
        taskName = arrWithItemsForTaskList[arrWithItemsForTaskListLastItem].dataset.taskName
        arrWithItemsForTaskList[arrWithItemsForTaskListLastItem].addEventListener("click", () => showDetailedInformationAboutTask(taskName))
    }
}

function getArrWithItemsForTaskList(arrWithItemsForAllTaskList, taskListId) {
    let arr = [];
    for (let i=0;i<arrWithItemsForAllTaskList.length;i++){
        if(arrWithItemsForAllTaskList[i].dataset.tasklistId == taskListId ){
            arr.push(arrWithItemsForAllTaskList[i])
        }     
    } 
    return arr
}

function showDetailedInformationAboutTask(taskName) {
    const informationPlace = document.querySelector(`.main`);
    const htmlCode = getDetailedInformationAboutTaskHTMLTemplate(taskName)
    informationPlace.insertAdjacentHTML("afterbegin",htmlCode )       
    document.querySelector(".information_header_close_btn").addEventListener("click", closeInformation)    
}

function closeInformation() {
    const closeInformationplace = document.querySelector(".information");
    closeInformationplace.remove();
}
