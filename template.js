function getHtmlCodeForAllTaskList(arr) {
    let htmlCode ="";
    for(let i=0; i<arr.length;i++){
        htmlCode +=`<div class="main_taskList">
                        <div class="taskList_header">
                            <div class="taskList_header_name">${arr[i].title}</div>
                            <div class="taskList_header_settings">
                                <div class="settings_points">...</div>
                                <input id="buttonDelete" type="button" value="Delete" class="settings_delete_btn" hidden>
                            </div>
                        </div>    
                        <div class="task_input" hidden><input type="text" class="input"></div>
                        <div class="dropdown" hidden>
                            <div class="dropdown_arrow">&#8744;</div>
                            <div class="dropdown_list">
                            </div>
                        </div>
                        <div><button class="button_add_card button_add_card_hover">+Add card</buttton></div>
                        <div class="button_submit" hidden><input id="button" type="button" value="Submit" class="btn_submit "></div>
                    </div>`
        }
    return htmlCode
}
function getTaskListItemHTMLTemplate (taskName, taskListId = 0, taskId) {
    return `<div class="task" data-task-name="${taskName}" data-tasklist-id="${taskListId}" data-task-id="${taskId}" draggable="true">
        ${taskName}
    </div>`
}

function getTaskListItemHTMLTemplateInDropDown( previousTaksList, i) {
    let htmlCodeInDropDown =``;
    htmlCodeInDropDown += `<div class="dropdown_task" data-task-dropdown-name = "${previousTaksList.issues[i].name}" data-task-dropdown-id="${previousTaksList.issues[i].id}">
                                    ${previousTaksList.issues[i].name}
                                </div>` 
    return  htmlCodeInDropDown                                                     
}


function getDetailedInformationAboutTaskHTMLTemplate(taskName){
        return  `<div class="information">
                    <div class="information_header">
                        <div class="information_header_name">${taskName}</div>
                        <div class="information_header_close"><button class="information_header_close_btn">&#10006;</button></div>
                    </div>
                    <div class="information_text">Information</div>
                </div>`
            
}

function getHtmlCodeInActiveTask(counter){
    return `Active task :${counter}`;
}
function getHtmlCodeInFinishedTask(counter){
    return `Finished task :${counter}`;
}

function getHtmlCodeForAddNewTaskList (){
    return      `<div class="main_taskList">
                    <div class="task_input" ><input type="text" placeholder="Name Task List" class="newTask_input"></div>
                    <div class="button_submit"><input id="buttonNewList" type="button" value="Submit" class="btn_submit "></div>
                </div>`
 
}

function getHtmlAllTaskListDelete () {
    return  `<div class="information">
                У вас нет списков задач. Добавьте новые.        
            </div>`
}